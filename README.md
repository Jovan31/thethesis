#PhD Thesis: Globular clusters in the Local Group as probes of galaxy assembly #

### Scientific chapters published in: ###

* http://iopscience.iop.org/article/10.1088/2041-8205/768/2/L33/pdf
* http://mnras.oxfordjournals.org/content/435/4/3654.full.pdf
* http://mnras.oxfordjournals.org/content/442/4/2929.full.pdf
* http://mnras.oxfordjournals.org/content/452/1/320.full.pdf

### Lay Summary ###

Understanding the formation and evolution of galaxies is one of the most active areas of research in astrophysics. Gradual build-up of matter by merging pre-galactic fragments is the current preferred model of constructing massive galaxies. A key prediction of this theory is that outskirts nearby galaxies should contain remnants of this assembly process in the form of fragmented stellar streams. 

Found in all but the smallest of galaxies, globular star clusters (GC) are excellent probes for studying properties of galaxies. Having high luminosities, they are favourable targets in the outer regions of galaxies where the associated stellar surface brightness is low. GCs are thought to be amongst the oldest stellar systems in the Universe, and are likely born in the most significant phases of galaxy formation. Their metal abundances, ages, spatial positions and motions can be used to constrain the assembly history of their host galaxy. 

In this thesis, I explore in great detail the motions and the light coming from GC systems in several nearby galaxies. The work is based on a major spec- troscopic campaign, follow-up to the Pan-Andromeda Archaeological Survey (PAndAS), a large imaging program, designed to observe the Andromeda galaxy. Line-of-sight velocities are obtained for 78 GCs in the far outskirts of Andromeda, 63 of which were not studied before. In addition, GCs in the dwarf galaxies NGC 147, NGC 185 and NGC 6822 are also spectroscopically observed. 

By conducting a detailed analysis, I find that GCs in the remote regions of Andromeda exhibit significant degree of rotation, which is in the same direction as for the GC located in the heart of the galaxy. My analysis of the GC motions located in the outskirts of Andromeda give further clues about how this galaxy got assembled through merging of smaller dwarf galaxies. I also estimate the total mass of Andromeda using the motions of its remote GC system. 

I also characterize the GC systems of three dwarf galaxies in the Local Group: the dwarf elliptical satellites of M31, NGC 147 and NGC 185, and the isolated dwarf irregular NGC 6822. Using uniform optical and near-IR imagery, I constrain the age and metal abundance of their constituent GCs. The GCs around NGC 147 and NGC 185 are found to lack metals, as is typically the case for these type of objects, while their ages are more difficult to constrain. On the other hand, the GCs hosted by NGC 6822 are found to be very old, but with a variety of metal abundances. Finally, I analyse the motions of the GCs in these three systems, and use them to constrain the masses of the respective host galaxies.