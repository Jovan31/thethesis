% !TEX root = ../master.tex
\chapter{Review of the NGC 147 and NGC 185 GC discovery history}
\label{ap:GChistory}

\begin{chapterpublicationdeclaration}
\emph{The contents of this appendix are published in \citet{Veljanoski13b}}
\end{chapterpublicationdeclaration}

The following is a short review of the discovery history and the nomenclature of the GCs hosted
by NGC 147 and NGC 185. The motivation for this is to highlight some inconsistencies in the
literature that were discovered while this thesis was in preparation. This is done in order to
minimise the possibility of future confusion when studying the GCs of these two dEs.

The existence of GCs in both NGC 147 and NGC 185 was first reported by \citet{Baade44}, who
discovered two GCs in each of the galaxies. In his paper, the clusters were not named and
coordinates were provided only for the ones hosted by NGC 185 and in terms of relative positions
(measured on photographic plates) from the galaxy centre.

\citet{Hodge74} reported the discovery and presented photometry of five GCs in NGC 185, two of
which were those previously discovered by \citet{Baade44}. These clusters were simply labelled 1-5.
While their coordinates were not provided, a finding chart was shown.

Two years later, \citet{Hodge76} published a paper on the structure of NGC 147, in which the
discovery and photometry of two additional GCs bound to this galaxy were presented, alongside the
two clusters previously discovered by \citet{Baade44}. Once again, no coordinates for any of these
objects were given, but a finding chart was published, on which the clusters are labelled 1-4. In
the literature, these clusters are now known as Hodge I-IV.

In an appendix to their paper on planetary nebulae in NGC 147 and NGC 185, \citet{FJJ77} revisited
the GC systems of the two dEs. In addition to the GCs already discovered by Baade and Hodge around
NGC 185, they presented the discovery of an additional four clusters from their photographic
plates, while also discarding the object labelled by \citet{Hodge74} as ``2'' from being a GC. In
\citet{FJJ77}, the objects are numbered I-VIII but the counting does not follow the pattern
started by \citet{Hodge74}. This nomenclature has propagated through the literature, and these
clusters are referred to as FJJ I-VIII in recent publications. In addition, equatorial coordinates
together with a finding chart were also published for the entire sample of GCs described by
\citet{FJJ77}.

\citet{FJJ77} also revisited the GC system of NGC 147. They recovered the objects already
identified by \citet{Hodge76} as globulars and did not find any new members belonging to this
system. They showed a finding chart and a table with equatorial coordinates for the three brightest
globulars in this galaxy. Their finding chart is identical to the one published by \citet{Hodge76}
in terms of the labelling and positions of the GCs: Hodge II is south of Hodge I and Hodge III
is south of Hodge II. However, in their Table 9 that lists the clusters' coordinates, the positions
of Hodge II and Hodge III are swapped: Hodge II is listed to be south of Hodge III. This
unintentional oversight is most probably the main reason for many inconsistencies in the more
recent literature.

A paper published by \citet{daCostaMould88} presented spectroscopic data and metal abundances of
the GCs hosted by NGC 147 and NGC 185. Regarding the NGC 185 system, they presented V-band
photometry taken from \citet{Hodge74} and spectroscopic data for clusters FJJ I-V, as well as
Hodge 2 that \citet{FJJ77} classified not to be a cluster.  For cluster FJJ IV which was not listed
amongst the GC candidates by \citet{Hodge74} the V magnitude was estimated by eye. Analysing the
cluster spectra, \citet{daCostaMould88} showed that Hodge 2 is indeed a galaxy at redshift $z=0.04$
and not a GC.

Regarding the NGC 147 system, \citet{daCostaMould88} took spectra only of Hodge I and Hodge III.
In their Table 1, they listed the photometric V magnitude of these two clusters as reported by
\citet{Hodge76}. They did not list coordinates for any of the clusters but stated that the centres
of the clusters were taken from \citet{FJJ77}.  This probably means that they presented metal
abundances of Hodge II rather than Hodge III.  There was no indication that
\citet{daCostaMould88} noticed the oversight made by \citet{FJJ77}, and in Table \ref{tab:cal}
I have assumed that they have not.

\citet{Geisler99} observed all but one cluster in NGC 185 with the HST, that were known at that
time. The cluster which was not  observed was FJJ VIII. They found that FJJ VI is not a GC but
an  elliptical galaxy. All other cluster candidates that were observed were confirmed to be
genuine GCs.

In more recent literature, \citet{sharina06ages} revisited the GC systems of NGC 147 and NGC 185
using HST/WFPC2 imagery and spectroscopy taken with the SCORPIO spectrograph. They did not
provide coordinates for any of the clusters, but they did provide finding charts. In the case of
NGC 147, they had the positions of Hodge II and Hodge III reversed compared to the original
publication by \citet{Hodge76}, so it is highly likely the data and results presented for Hodge II
actually refer to Hodge III and vice versa.  In their Table 2, they have taken the V magnitudes of
what they label as Hodge I and Hodge III from the original paper by \citet{Hodge76},  while the V
magnitude of their Hodge II is taken from \citet{Hodge74} even though this paper analysed only
clusters hosted by NGC 185 and did not list photometric values for any cluster in NGC 147. This
made the cluster they label as Hodge II brighter than Hodge III, which is easily seen not to be the
case with simple visual inspection of their HST images.

The most recent publication regarding the NGC 147 GC system is by \citet{SharinaDavoust09}. In
their paper they announced the discovery of three new GCs. They presented a coordinate table and a
finding chart, both having correct positions compared to the original \citet{Hodge76} publication
regarding the ``classical Hodge'' clusters. In addition, they also state that the identifiers of
Hodge II and Hodge III were inverted by mistake in \citet{sharina06ages}, meaning any values cited
from the earlier publication are not  assigned to the wrong object in \citet{SharinaDavoust09}.

Finally, in this theis  I report the discovery of another three GCs hosted by NGC 147, and one
hosted by NGC 185. Interestingly, all three of the new GCs in NGC 147 lie beyond areas previously
imaged for GC searches, while the new GC in NGC 185 lies within the photographic plate region
searched by \citet{FJJ77}. It can be speculated that the outlying nature of this object coupled
with its low luminosity caused it to be missed in the original study. The tables presented in this
thesis reflect the original naming and correct coordinates for all previously known GCs, as well as
these new discoveries.