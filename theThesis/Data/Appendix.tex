% !TEX root = ../master.tex
\chapter{Radial velocity technique comparison}
\label{ch:appendix}

Radial velocities, and more recently redshifts, are one of the fundamental measurements done when
studying astrophysical objects. Knowledge of the motions of celestial objects is extremely
important and it has diverse applications in both the local and distant Universe.

 It was first recognised by \citet{Doppler41} that the radial motion would change the observed
 colour of a star, a process analogous to that of changing the pitch of sound when there is a
 relative motion between the source and the detector. \citet{Fizeau48,Fizeau41} realised
 that this would mean a shift in the positions of the Fraunhofer lines. The first visual attempt to
 observe these shifts was made by \citet{Huggins68}, who attempted to determine whether a sample
 of stars and nebulae are moving toward or away from the Earth.

Influenced by radar studies during World War II, \citet{Fellgett55} suggested correlation
techniques for deriving radial velocities, which were then implemented by \citet{Griffin67}.

With the advent of digital detectors and the Fast Fourier Transformation (FFT) algorithm
\citep{Cooley65}, the use of digital power spectrum techniques finally became practical
\citep{Blackman58}. It was then shown by \citet{Simkin74} that Fourier methods could be used to
derive radial velocities from digital spectroscopic data. The method proposed by \citet{Simkin74}
was first put to practical use by \citet{Sargent77gal}, who used it to determine the redshifts and
velocity dispersions of a sample containing 13 galaxies.

\citet{TonryDavis79} combined the use of power spectrum techniques along with the use of the FFT
algorithm to devise a method for obtaining redshifts and radial velocities from digital
spectroscopic data. In addition, they invented the \emph{r} statistic, which is calibrated to give
the error of a measurement. Today, this method by \citet{TonryDavis79} is by far the most popular
for determining radial velocities for digital spectra, especially for star clusters and galaxies,
since it is fast and reliable in many cases.

However, if a noise feature is misidentified to be a real spectral line, the cross correlation
FFT method may produce incorrect results. One way to avoid such misidentification is to use the
accompanying error spectrum as a means to better determine if a feature in a spectrum is a real
line or noise. This served as the motivation behind the construction of the $\chi^2$ technique
described in Section \ref{sec:rvm}. The $\chi^2$ technique essentially performs the same analysis
as the \citet{TonryDavis79} method, but done in real space while also weighting each pixel element
of the studied spectrum by the inverse of its accompanying uncertainty, effectively reducing the
chances of a noise-induced feature to be misidentified as a real line in the process.

\begin{figure}
\centering
\includegraphics[scale = 0.59]{Data/figures/comparison-xcsao.eps}
\caption[\ Comparing the $\chi^2$ routine and \emph{xcsao} velocity measurements]{Comparing
the radial velocities derived with the $\chi^2$ routine to those obtained with
\emph{xcsao} task in {\sc iraf} \citep{Kurtz98}. The comparison is done for the M31 halo GC
sample. In general there is excellent agreement between the two sets of measurements with few
just a few catastrophic misidentification by the \emph{xcsao} task.}
\label{fig:xcsao-comp}
\end{figure}

To test the performance of the $\chi^2$ routine, I compare it against the velocities of the M31
halo GCs derived with the \emph{xcsao} task part of the {\sc rvsao} package \citep{Kurtz98}. This
task is an advanced implementation of the \citet{TonryDavis79} algorithm coded in the {\sc iraf}
environment. Figure \ref{fig:xcsao-comp} shows the comparison between the two techniques, and
the data is listed in Table \ref{tab:rvtable-xcao}. There is an excellent agreement between the two
techniques, with only a few discrepant points. Investigating the corresponding $\chi^2$ and cross
correlation functions for the deviant points, I found that in all such cases a series of noise
features caused \emph{xcsao} to produce a spurious cross correlation peak resulting in an incorrect
velocity. Setting \emph{xcsao} to ignore the noise infested regions produces velocities consistent
with those from the $\chi^2$ routine. In addition, the $\chi^2$ routine is found have marginally
better precision compared to \emph{xcsao}. Over the whole M31 halo radial velocity sample, the mean
uncertainty from the $\chi^2$ routine measurements is 10 \kms, while for \emph{xcsao} is 13 \kms.



\begin{table*}
\centering
\footnotesize
\caption[\ Comparing the $\chi^2$ and cross-correlation velocity techniques ]{Comparison
between the radial velocity measurements of the M31 halo GC sample derived with the custom
$\chi^2$ code to those derived with the \emph{xcsao} task \citep{Kurtz98} in {\sc iraf}
which employs the cross correlation method as devised by \citet{TonryDavis79}. }
\label{tab:rvtable-xcao}
\begin{tabular}{llllllll}
\hline
\hline
Cluster ID      & RA (J2000)          & DEC (J2000)     & $\Rproj$      & PA           & GC type       &     $V_{\chi^2 {\rm routine}}$            & $V_{\rm xcsao}$                \\
                & [hh:mm:sss]         &[dd:mm:sss]      & [kpc]         & [deg]        &               &     [km s$^{-1}$]                         & [km s$^{-1}$]                  \\
\hline
B514            & +00:31:09.8         & +37:54:00       &  55           & 214          & GC            &     -471    $\pm$  8          &     -471    $\pm$     9     \\
B517            & +00:59:59.9         & +41:54:06       &  45           & 78           & GC            &     -277    $\pm$  13         &     -262    $\pm$     15    \\
EXT8            & +00:53:14.5         & +41:33:24       &  27           & 81           & GC            &     -194    $\pm$  6          &     -185    $\pm$     11    \\
G001            & +00:32:46.5         & +39:34:40       &  35           & 229          & GC            &     -335    $\pm$  5          &     -335    $\pm$     5     \\
G002            & +00:33:33.7         & +39:31:18       &  34           & 226          & GC            &     -352    $\pm$  19         &     -358    $\pm$     21    \\
G268            & +00:44:10.0         & +42:46:57       &  21           & 10           & GC            &     -277    $\pm$  8          &     -290    $\pm$     9     \\
G339            & +00:47:50.2         & +43:09:16       &  29           & 26           & GC            &     -97     $\pm$  6          &     -100    $\pm$     12    \\
H1              & +00:26:47.7         & +39:44:46       &  46           & 245          & GC            &     -245    $\pm$  7          &     -240    $\pm$     11    \\
H2              & +00:28:03.2         & +40:02:55       &  42           & 248          & GC            &     -519    $\pm$  16         &     -512    $\pm$     12    \\
H3              & +00:29:30.1         & +41:50:31       &  35           & 284          & GC            &     -86     $\pm$  9          &     -84     $\pm$     9     \\
H4              & +00:29:44.9         & +41:13:09       &  33           & 270          & GC            &     -368    $\pm$  8          &     -363    $\pm$     13    \\
H5              & +00:30:27.2         & +41:36:19       &  32           & 279          & GC            &     -392    $\pm$  12         &     -395    $\pm$     12    \\
H7              & +00:31:54.5         & +40:06:47       &  32           & 242          & GC            &     -426    $\pm$  23         &     -000    $\pm$     25    \\
H8              & +00:34:15.4         & +39:52:53       &  29           & 230          & GC            &     -463    $\pm$  3          &     -462    $\pm$     5     \\
H9              & +00:34:17.2         & +37:30:43       &  56           & 204          & GC            &     -374    $\pm$  5          &     -359    $\pm$     2     \\
H10             & +00:35:59.7         & +35:41:03       &  78           & 194          & GC            &     -352    $\pm$  9          &     -356    $\pm$     12    \\
H11             & +00:37:28.0         & +44:11:26       &  42           & 342          & GC            &     -213    $\pm$  7          &     -215    $\pm$     9     \\
H12             & +00:38:03.8         & +37:44:00       &  50           & 195          & GC            &     -396    $\pm$  10         &     -389    $\pm$     12    \\
H14             & +00:38:49.4         & +42:22:47       &  18           & 327          & GC            &     -271    $\pm$  15         &     -269    $\pm$     12    \\
H15             & +00:40:13.2         & +35:52:36       &  74           & 185          & GC            &     -367    $\pm$  10         &     -286    $\pm$     13    \\
H17             & +00:42:23.6         & +37:14:34       &  55           & 181          & GC            &     -246    $\pm$  16         &     -244    $\pm$     14    \\
H18             & +00:43:36.0         & +44:58:59       &  51           & 2.4          & GC            &     -206    $\pm$  21         &     -212    $\pm$     24    \\
H19             & +00:44:14.8         & +38:25:42       &  39           & 174          & GC            &     -272    $\pm$  18         &     -268    $\pm$     20    \\
H22             & +00:49:44.6         & +38:18:37       &  44           & 155          & GC            &     -311    $\pm$  6          &     -293    $\pm$     13    \\
H23             & +00:54:24.9         & +39:42:55       &  37           & 124          & GC            &     -377    $\pm$  11         &     -381    $\pm$     12    \\
H24             & +00:55:43.9         & +42:46:15       &  39           & 57           & GC            &     -121    $\pm$  15         &     -110    $\pm$     12    \\
H25             & +00:59:34.5         & +44:05:38       &  57           & 46           & GC            &     -204    $\pm$  14         &     -207    $\pm$     18    \\
H26             & +00:59:27.4         & +37:41:30       &  66           & 137          & GC            &     -411    $\pm$  7          &     -407    $\pm$     12    \\
H27             & +01:07:26.3         & +35:46:48       &  100          & 137          & GC            &     -291    $\pm$  5          &     -277    $\pm$     7     \\
HEC1            & +00:25:33.8         & +40:43:38       &  45           & 262          & EC            &     -233    $\pm$  9          &     -225    $\pm$     13    \\
HEC2            & +00:28:31.5         & +37:31:23       &  63           & 217          & EC            &     -341    $\pm$  9          &     -98     $\pm$     18    \\
HEC6            & +00:38:35.4         & +44:16:51       &  42           & 346          & EC            &     -132    $\pm$  12         &     -209    $\pm$     20    \\
HEC10           & +00:54:36.4         & +44:58:44       &  59           & 29           & EC            &     -98     $\pm$  5          &     -366    $\pm$     10    \\
HEC11           & +00:55:17.4         & +38:51:01       &  47           & 134          & EC            &     -215    $\pm$  5          &     -341    $\pm$     13    \\
HEC13           & +00:58:17.1         & +37:13:49       &  69           & 142          & EC            &     -366    $\pm$  5          &     -132    $\pm$     10    \\
MGC1            & +00:50:42.4         & +32:54:58       &  116          & 169          & GC            &     -355    $\pm$  7          &     -355    $\pm$     7     \\
\hline
\end{tabular}
\end{table*}



\addtocounter{table}{-1}
\begin{table*}
\centering
\footnotesize
\caption[]{Continued}
\begin{tabular}{llllllll}
\hline
\hline
Cluster ID      & RA (J2000)          & DEC (J2000)     & $\Rproj$      & PA        & GC type       &     $V_{\chi^2 {\rm routine}}$& $V_{\rm xcsao}$                \\
                & [hh:mm:sss]         &[dd:mm:sss]      & [kpc]         & [deg]     &               &     [km s$^{-1}$]             & [km s$^{-1}$]                 \\
\hline
PAndAS-01       & +23:57:12.0         & +43:33:08       &  119          & 289       & GC            &     -333    $\pm$  21         &     -309    $\pm$      24    \\
PAndAS-02       & +23:57:55.6         & +41:46:49       &  115          & 277       & EC            &     -266    $\pm$  4          &     -267    $\pm$      7     \\
PAndAS-04       & +00:04:42.9         & +47:21:42       &  125          & 315       & GC            &     -397    $\pm$  7          &     -390    $\pm$      9     \\
PAndAS-05       & +00:05:24.1         & +43:55:35       &  101          & 294       & EC            &     -183    $\pm$  7          &     -182    $\pm$      10    \\
PAndAS-06       & +00:06:11.9         & +41:41:20       &  94           & 277       & GC            &     -327    $\pm$  15         &     -363    $\pm$      9     \\
PAndAS-07       & +00:10:51.3         & +39:35:58       &  86           & 257       & EC            &     -452    $\pm$  18         &     -453    $\pm$      15    \\
PAndAS-08       & +00:12:52.4         & +38:17:47       &  88           & 245       & GC            &     -416    $\pm$  8          &     -418    $\pm$      11    \\
PAndAS-09       & +00:12:54.6         & +45:05:55       &  91           & 308       & GC            &     -444    $\pm$  21         &     -592    $\pm$      16    \\
PAndAS-10       & +00:13:38.6         & +45:11:11       &  90           & 309       & EC            &     -435    $\pm$  10         &     -436    $\pm$      10    \\
PAndAS-11       & +00:14:55.6         & +44:37:16       &  83           & 306       & GC            &     -447    $\pm$  13         &     -446    $\pm$      14    \\
PAndAS-12       & +00:17:40.0         & +43:18:39       &  69           & 296       & EC            &     -472    $\pm$  5          &     -471    $\pm$      14    \\
PAndAS-13       & +00:17:42.7         & +43:04:31       &  68           & 293       & GC            &     -570    $\pm$  45         &     -610    $\pm$      65    \\
PAndAS-14       & +00:20:33.8         & +36:39:34       &  86           & 225       & EC            &     -363    $\pm$  9          &     -374    $\pm$      14    \\
PAndAS-15       & +00:22:44.0         & +41:56:14       &  52           & 282       & GC            &     -385    $\pm$  6          &     -385    $\pm$      6     \\
PAndAS-16       & +00:24:59.9         & +39:42:13       &  51           & 247       & GC            &     -490    $\pm$  15         &     -391    $\pm$      39    \\
PAndAS-17       & +00:26:52.2         & +38:44:58       &  54           & 232       & GC            &     -279    $\pm$  15         &     -265    $\pm$      12    \\
PAndAS-18       & +00:28:23.2         & +39:55:04       &  42           & 245       & EC            &     -551    $\pm$  18         &     -546    $\pm$      29    \\
PAndAS-19       & +00:30:12.2         & +39:50:59       &  38           & 240       & GC            &     -544    $\pm$  6          &     -542    $\pm$      14    \\
PAndAS-21       & +00:31:27.5         & +39:32:21       &  38           & 232       & GC            &     -600    $\pm$  7          &     -595    $\pm$      13    \\
PAndAS-22       & +00:32:08.3         & +40:37:31       &  29           & 253       & GC            &     -437    $\pm$  1          &     -436    $\pm$      1     \\
PAndAS-23       & +00:33:14.1         & +39:35:15       &  34           & 228       & GC            &     -476    $\pm$  5          &     -475    $\pm$      7     \\
PAndAS-27       & +00:35:13.5         & +45:10:37       &  57           & 341       & GC            &     -46     $\pm$  8          &     -37     $\pm$      9     \\
PAndAS-36       & +00:44:45.5         & +43:26:34       &  30           & 9         & GC            &     -399    $\pm$  7          &     -403    $\pm$      12    \\
PAndAS-37       & +00:48:26.5         & +37:55:42       &  48           & 161       & GC            &     -404    $\pm$  15         &     -398    $\pm$      13    \\
PAndAS-41       & +00:53:39.5         & +42:35:14       &  33           & 56        & GC            &     -94     $\pm$  8          &     -111    $\pm$      13    \\
PAndAS-42       & +00:56:38.0         & +39:40:25       &  42           & 120       & GC            &     -176    $\pm$  4          &     -176    $\pm$      9     \\
PAndAS-43       & +00:56:38.8         & +42:27:17       &  39           & 64        & GC            &     -135    $\pm$  6          &     -136    $\pm$      5     \\
PAndAS-44       & +00:57:55.8         & +41:42:57       &  39           & 80        & GC            &     -349    $\pm$  11         &     -330    $\pm$      10    \\
PAndAS-45       & +00:58:37.9         & +41:57:11       &  42           & 76        & GC            &     -135    $\pm$  16         &     -135    $\pm$      16    \\
PAndAS-46       & +00:58:56.3         & +42:27:38       &  44           & 67        & GC            &     -132    $\pm$  16         &     -120    $\pm$      15    \\
PAndAS-47       & +00:59:04.7         & +42:22:35       &  44           & 69        & GC            &     -359    $\pm$  16         &     -354    $\pm$      18    \\
PAndAS-48       & +00:59:28.2         & +31:29:10       &  141          & 160       & EC            &     -250    $\pm$  5          &     -250    $\pm$      5     \\
PAndAS-49       & +01:00:50.0         & +42:18:13       &  48           & 72        & EC            &     -240    $\pm$  7          &     -234    $\pm$      12    \\
PAndAS-50       & +01:01:50.6         & +48:18:19       &  107          & 24        & EC            &     -323    $\pm$  7          &     -323    $\pm$      7     \\
PAndAS-51       & +01:02:06.6         & +42:48:06       &  53           & 65        & GC            &     -226    $\pm$  5          &     -226    $\pm$      14    \\
PAndAS-52       & +01:12:47.0         & +42:25:24       &  78           & 76        & GC            &     -297    $\pm$  9          &     -301    $\pm$      15    \\
PAndAS-53       & +01:17:58.4         & +39:14:53       &  96           & 104       & GC            &     -253    $\pm$  10         &     -275    $\pm$      9     \\
PAndAS-54       & +01:18:00.1         & +39:16:59       &  96           & 104       & GC            &     -336    $\pm$  8          &     -341    $\pm$      10    \\
PAndAS-56       & +01:23:03.5         & +41:55:11       &  103          & 82        & GC            &     -239    $\pm$  8          &     -248    $\pm$      10    \\
PAndAS-57       & +01:27:47.5         & +40:40:47       &  116          & 90        & GC            &     -186    $\pm$  6          &     -184    $\pm$      6     \\
PAndAS-58       & +01:29:02.1         & +40:47:08       &  119          & 89        & GC            &     -167    $\pm$  10         &     -170    $\pm$      12    \\
SK255B          & +00:49:03.0         & +41:54:57       &  18           & 61        & GC            &     -191    $\pm$  10         &     -193    $\pm$      17    \\
\hline
\end{tabular}
\end{table*}