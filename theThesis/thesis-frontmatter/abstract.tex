\chapter{Abstract}

Understanding the formation and evolution of galaxies is one of the most active areas of research
in astrophysics. Hierarchical merging of proto-galactic fragments to build more massive galaxies
is the current preferred model. A key prediction of this theory is that haloes of nearby galaxies
should contain remnants of this assembly process in the form of tidal debris.

Found in all but the smallest of dwarf galaxies, globular clusters (GC) are excellent probes of
galaxy haloes. Having high luminosities, they are favourable targets in the outer regions of
galaxies where the associated stellar surface brightness is low. GCs are thought to be amongst
the oldest stellar systems in the Universe, and are likely born in the most significant phases of
galaxy formation. Their metallicities, ages, spatial distributions and kinematics can be used to
constrain the assembly history of their host galaxy.

In this thesis, I explore the photometric and kinematic properties of several GC systems in our
cosmological backyard, the Local Group of galaxies. The work is based on a major spectroscopic
campaign, follow-up to the photometric Pan-Andromeda Archaeological Survey (PAndAS), as well as
additional optical and near-IR data sets. Radial velocities are obtained for 78 GCs in the halo of
M31, 63 of which had no previous spectroscopic information. The GCs have projected radii between
$\sim 20 $ and 140~kpc, thus sampling the true outer halo of this galaxy. In addition, GCs in the
dwarf galaxies NGC~147, NGC~185 and NGC~6822 are also spectroscopically observed.

By conducting a detailed kinematic analysis, I find that GCs in the outer halo of M31 exhibit
coherent rotation around the minor optical axis, in the same direction as their more centrally
located counterparts, but with a smaller amplitude of $86\pm17$~\kms. There is also evidence
that the velocity dispersion of the outer halo GC system decreases as a function of projected
radius from the M31 centre, and this relation can be well described by a power law of index
$\approx -0.5$. I detect and discuss various velocity correlations amongst GCs that lie on stellar
streams in the M31 halo. Simple Monte Carlo tests show that such configurations are unlikely to
form by chance, implying that significant fraction of the GCs in the M31 halo have been accreted
alongside their parent dwarf galaxies. I also estimate the dynamical mass of M31 within 200~kpc to
be $(1.2 - 1.6) \pm 0.2 \times 10^{12}$~\Msun.

I also characterize the GC systems of three dwarf galaxies in the Local Group: the dwarf elliptical
satellites of M31, NGC~147 and NGC~185, and the isolated dwarf irregular NGC~6822. Using uniform
optical and near-IR photometry, I constrain the ages and metallicities of their constituent GCs.
The metallicities of the GCs around NGC~147 and NGC~185 are found to be metal-poor
([Fe/H]$\lesssim-1.25$~dex), while their ages are more difficult to constrain. The GCs hosted by
NGC~6822 are found to be old ($>$9~Gyr) and to have a spread of metallicities
(${\rm -1.6 \lesssim [Fe/H] \lesssim -0.4}$). I find close similarity between the mean optical
$(V-I)_0$~colours of the GCs hosted by these three dwarf galaxies to those located in the M31 outer
halo, consistent with the idea that dwarf galaxies akin to them might have contributed toward the
assembly of the M31 outer halo GC population. Analysing their kinematics, I find no evidence for
systemic rotation in either of these three GC systems. Finally, I use the available GC kinematic
data to calculate the dynamical masses of NGC~147, NGC~185 and NGC~6822.