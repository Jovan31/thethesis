% !TEX root = ../master.tex
\chapter{Summary}

% \begin{chapterpublicationdeclaration}
% \textsf{This chapter has been published in \citet{Veljanoski13a}.}
% \end{chapterpublicationdeclaration}

The formation and evolution of galaxies through cosmic time are fundamental topics in astrophysics.
One way of studying the assembly of a galaxy is through its globular star clusters (GCs). These
bright, compact objects are thought to be amongst the oldest stellar systems in the Universe, formed
in the most significant phases of galaxy formation. Various correlations have been found between
the properties of GC systems and those of their parent galaxies, which makes them valuable tracers
of galaxy formation and evolution.

The focus of this thesis is our cosmological backyard, the Local Group of galaxies. Initially
the spotlight was laid on the outer halo GC system of M31, and then shifted to the GC systems of
the dwarf galaxies NGC~147, NGC~147 and NGC~6822. By conducting photometric and kinematic analysis,
important links were found between the GCs hosted by these dwarf galaxies to those located in the
M31 outer halo, consistent with the idea that dwarf galaxies such as these are at least partially
responsible for the formation of massive galaxies akin to M31.

\section{Summary of science chapters}

\subsubsection{Kinematics of the M31 outer halo GC system}
This chapter presents a comprehensive kinematic analysis using radial velocities of 78 GCs
around M31, 63 of which had no previous spectroscopic information. The sample extends from
$\sim 20$ kpc out to $\sim 140$ kpc in projection, and at least up to 200 kpc in 3D, enabling the
exploration of the true outer halo GC system of M31.

A significant degree of net rotation is detected in the outer halo GC population of M31.
This population shares the the same rotation axis and direction as the GCs located in the inner
regions of M31, as well as the M31 disk. Evidence for decreasing velocity dispersion as a function
of projected distance from the M31 centre is also found. This behaviour of the velocity dispersion
is well described by a power-law. The dispersion profile of the halo GC population is similar to
that of the stellar halo, consistent with previous observations that the GCs and stars share
similar spatial density profiles.

The velocity measurements further revealed a variety of correlations for the groups of GCs
that lie projected on top of distinct stellar debris features in the field halo. This further
supports the idea that a significant fraction of the M31 halo GC system has an external origin
\citep[e.g.][]{Mackey10b}.

Finally, using the halo GCs as kinematic tracers, the total mass of M31 enclosed within a
3D radius of 200 kpc was estimated via the Tracer Mass Estimator. The calculated value of
$(1.2-1.6)\pm0.2 \times 10^{12} M_{\odot}$ is in agreement with other recent mass estimates which
employed kinematic tracers extending to similar radii.

\subsubsection{The GC systems of NGC~147 and NGC~185}

The focus of this chapter are the GC systems of NGC~147 and NGC~185. Exploring the PAndAS data
resulted in the discovery of 3 new GCs around NGC~147, and one near NGC~185. The new discoveries
update the census of GCs to 10 and 8 in NGC~147 and NGC~185, respectively.

Optical and near-IR photometric measurements are made on homogeneous data, as a means to constrain
the ages and metallicities of all GCs hosted by these two galaxies. In general, it is found that
the clusters are metal-poor ([Fe/H]$\lesssim-1.25$ dex), while their ages are more difficult to
constrain. In addition, a hint of decreasing metallicity as a function of projected radius is also
tentatively observed.

The close similarity between the $(V-I)_0$ colours of the GCs belonging to NGC~147 and NGC~185 and
those belonging to the M31 outer halo is consistent with the idea that accretion of the former
could have contributed to the assembly of the latter. In addition, the mean colours of the GCs
hosted by these dwarf galaxies are found to lie at the peak of the colour distribution of the
GC systems belonging to dEs in the Fornax and Virgo galaxy clusters, despite a large difference in
the environments in which they reside. Their $S_N$ values are consistent with the trend of
increasing GC specific frequency with decreasing galaxy luminosity generally observed for dwarf
galaxies, regardless of their type, and in a variety of environments.

A kinematic analysis of these two GC systems is done by combining the obtained spectroscopic data
of the newly-discovered GCs with the radial velocities of the previously known clusters available
in the literature. No significant rotation is detected in either GC system, despite the
known rotation of the stellar component in both galaxies. The velocity data is further used to
constrain the dynamical masses of NGC~147 and NGC~185 via the Tracer Mass Estimator, finding
$(1.4-1.8) \times 10^{9} M_{\odot}$ and $(6.7-8.4) \times 10^{9} M_{\odot}$ for NGC~147 and NGC
185, respectively. Both of these values are considerably higher than any previous mass estimates.

\subsubsection{The GC system of NGC~6822}

A uniform optical and near-IR photometric study is presented for the GC system hosted by NGC~6822.
The photometric measurements are used to estimate the ages and metallicities of the GCs via
colour-colour plots and an empirical colour-metallicity relation. All GCs are found to have old
ages ($>9$ Gyrs), in agreement with past studies. The clusters are found to exhibit a range of
metallicities (${\rm -1.6 \lesssim [Fe/H] \lesssim -0.4}$), contrary to previous studies which
found them to be very metal-poor ([Fe/H]<$-2.0$).

The mean $(V-I)_0$ optical colours of the GCs in NGC~6822 are consistent with those of the GCs
located in the M31 outer halo. Thus it is possible that dwarf irregular galaxies akin to NGC~6822
were the source of extended GCs found in the halo of M31.

Finally, the kinematic properties of this GC system are explored using new velocities for 6 GCs,
2 of which had no previous spectroscopic information. The GC system is not found to exhibit any
rotation signature, despite the known rotation of the HI disk and the carbon stars spheroid of
NGC~6822. Using the GCs as dynamical mass tracers, the total mass of NGC~6822 is recalculated to
be $(1.4-1.8) \times 10^{10} M_{\odot}$. The corresponding mass-to-light ratio of this galaxy is
between 140 - 180, making NGC~6822 highly dominated by dark matter.


\section{Future work}

\subsubsection{The dynamics of the M31 outer halo GC system}

The results presented in this work open doors for more detailed study of the dynamics of
the M31 outer halo GC system. Through modelling of the orbits of the stellar streams using the GCs
that project on top of them as kinematic tracers, one can learn more about the progenitor dwarf
galaxies that contributed towards the formation of the M31 halo. Determining the orbits of multiple
tidal streams will help deduce whether their formation is consistent with a single, more massive
progenitor which got fragmented as it accreted onto M31, if each prominent stream originated from
a separately accreted dwarf galaxy, or a combination of the two. In the latter two scenarios,
knowledge of the satellite orbits will be an excellent test of the hypothesis that satellites are
accreted from few preferred directions on the sky \citep[e.g.][]{Lovell11}, argument used to
explain the planar arrangement of satellite galaxies observed both around M31 and around the Milky
Way.

The existence of multiple dynamical tracers -- HI gas, planetary nebulae, red and blue globular
clusters, stellar streams, dwarf galaxies -- which dominate at different projected radii from the
centre of M31 presents a unique opportunity to constrain the mass profile of this galaxy.


\subsubsection{The metal content of the M31 halo GCs}

A good fraction of the spectroscopic data of the M31 outer halo GC system that I have presented in
this thesis is appropriate for measurements of Lick indices, through which the ages and
metallicities of the clusters can be estimated \citep[e.g.][]{Schiavon12}. This will enable the
search for age and metallicity gradients in the halo GC system of M31, the existence of which will
help constrain the origin of the halo GCs. For instance, the GCs that are associated with the
Sagittarius dwarf galaxy, currently being accreted onto the Milky Way, are observed to have
systematically different metallicities, for their age, compared to the field Galactic GCs
\citep[e.g.][]{Dotter11}. Hence, metallicity information, in addition to spatial location and
kinematics, may help to discriminate between GCs accreted along with their parent dwarf galaxies
to those formed \emph{in situ}, especially in complicated regions with intersecting stellar
streams.

Combining the spectroscopic data with deep proprietary HST observations of the M31 outer halo GCs
presents an opportunity for devising a method of determining the horizontal branch morphology via
integrated spectra alone. The idea is to identify signature ratios of key indices, primarily the
Balmer features. This has been attempted previously with the Milky Way GCs with some success
\citep{Schiavon04}. However, long slit observations of GCs in the Galaxy are challenging because
one needs to make the slit width sufficiently large in order to cover significant portion of the
cluster. The pursuit of this project with the M31 halo GCs has the advantage that the spectra are
virtually unaffected by foreground Milky Way stars, and yet these objects are sufficiently close
for the data to be of excellent quality. Such a study represents a crucial step towards the
breaking of the age-metallicity degeneracy that arises due to the possible existence of hot old
stars, which are hot enough to mimic the young stellar population. This will be of great interest
when studying GCs of more distant systems for which obtaining colour-magnitude diagrams in not
possible.

\section{Final words}

Modern theory offers a good overall explanation on how the Universe was created and how it grows
and evolves with time. And yet, looking in our cosmological backyard, we observe much complexity
in galaxies that we do not yet understand. The work presented here serves as a prime example of how
studying galaxies through their globular cluster systems can make the picture of their formation
and evolution more complete. A true revolution in this field is expected with the launch of the
next generation of space observatories, such as the \emph{James Webb Space Telescope}, which would
enable us to trace the evolution of globular cluster systems through a larger time span, adding
stronger constrains on the assembly processes of their respective hosts.





































