\contentsline {chapter}{Lay Summary}{i}{chapter*.2}
\contentsline {chapter}{Abstract}{iii}{chapter*.3}
\contentsline {chapter}{Declaration}{v}{chapter*.4}
\contentsline {chapter}{Acknowledgements}{vi}{chapter*.5}
\contentsline {chapter}{Contents}{ix}{section*.6}
\contentsline {chapter}{List of Figures}{xiii}{section*.8}
\contentsline {chapter}{List of Tables}{xvi}{section*.10}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Globular Clusters}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}The globular cluster system of the Milky Way}{5}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}The bimodal nature of the Milky Way GC system}{6}{subsection.1.2.1}
\contentsline {subsubsection}{Metallicity and colour bimodality}{6}{subsection.1.2.1}
\contentsline {subsubsection}{Spatial distributions}{6}{figure.caption.15}
\contentsline {subsubsection}{Kinematics}{7}{figure.caption.16}
\contentsline {subsection}{\numberline {1.2.2}The age-metallicity relation of the Galactic GC system}{10}{subsection.1.2.2}
\contentsline {section}{\numberline {1.3}Extragalactic globular cluster systems}{12}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Specific frequency}{14}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}Colour bimodality}{17}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}Kinematics}{18}{subsection.1.3.3}
\contentsline {subsection}{\numberline {1.3.4}The origin of bimodality and galaxy formation models}{20}{subsection.1.3.4}
\contentsline {subsubsection}{The major merger model}{20}{subsection.1.3.4}
\contentsline {subsubsection}{The multiphase collapse model}{21}{subsection.1.3.4}
\contentsline {subsubsection}{The dissipationless accretion model}{22}{subsection.1.3.4}
\contentsline {subsubsection}{Cosmological formation of GCs}{23}{subsection.1.3.4}
\contentsline {section}{\numberline {1.4}The globular cluster system of M31}{25}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}The GCs in the inner regions of M31}{26}{subsection.1.4.1}
\contentsline {subsubsection}{Metallicity}{27}{subsection.1.4.1}
\contentsline {subsubsection}{Kinematics}{29}{figure.caption.28}
\contentsline {subsection}{\numberline {1.4.2}The Pan-Andromeda Archaeological Survey}{32}{subsection.1.4.2}
\contentsline {chapter}{\numberline {2}Observations, data reduction and velocity measurements}{42}{chapter.2}
\contentsline {section}{\numberline {2.1}The observed sample}{42}{section.2.1}
\contentsline {section}{\numberline {2.2}The Data reduction}{45}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}\emph {WHT} and \emph {KPNO} data}{45}{subsection.2.2.1}
\contentsline {subsubsection}{Data description}{45}{subsection.2.2.1}
\contentsline {subsubsection}{Data reduction and calibration}{46}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}\emph {Gemini} Data}{48}{subsection.2.2.2}
\contentsline {subsubsection}{Data description}{48}{subsection.2.2.2}
\contentsline {subsubsection}{Data reduction and calibration}{48}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Radial velocity measurements}{49}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Corrections for perspective}{53}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Summary}{56}{section.2.4}
\contentsline {chapter}{\numberline {3}Kinematics of the M31 halo globular clusters}{60}{chapter.3}
\contentsline {section}{\numberline {3.1}Bayesian inference}{60}{section.3.1}
\contentsline {section}{\numberline {3.2}Kinematic models}{62}{section.3.2}
\contentsline {section}{\numberline {3.3}Overall halo kinematics}{64}{section.3.3}
\contentsline {section}{\numberline {3.4}Globular cluster groups on streams}{68}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}The North-West stream}{71}{subsection.3.4.1}
\contentsline {subsection}{\numberline {3.4.2}The South-West Cloud}{73}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Streams C and D}{75}{subsection.3.4.3}
\contentsline {subsection}{\numberline {3.4.4}The Eastern Cloud}{77}{subsection.3.4.4}
\contentsline {subsection}{\numberline {3.4.5}Association 2}{78}{subsection.3.4.5}
\contentsline {subsection}{\numberline {3.4.6}The case of And\nobreakspace {}XVII}{80}{subsection.3.4.6}
\contentsline {section}{\numberline {3.5}The dynamical mass of M31}{81}{section.3.5}
\contentsline {section}{\numberline {3.6}Discussion}{85}{section.3.6}
\contentsline {section}{\numberline {3.7}Summary}{87}{section.3.7}
\contentsline {chapter}{\numberline {4}The globular cluster systems of NGC\nobreakspace {}147 and NGC\nobreakspace {}185}{88}{chapter.4}
\contentsline {section}{\numberline {4.1}The data}{90}{section.4.1}
\contentsline {section}{\numberline {4.2}Discovery of new GCs}{91}{section.4.2}
\contentsline {section}{\numberline {4.3}Photometry}{97}{section.4.3}
\contentsline {section}{\numberline {4.4}Ages and metallicities}{102}{section.4.4}
\contentsline {section}{\numberline {4.5}Kinematics}{106}{section.4.5}
\contentsline {subsubsection}{The kinematics of NGC\nobreakspace {}147 GC system}{108}{equation.4.5.8}
\contentsline {subsubsection}{The kinematics of the NGC\nobreakspace {}185 GC system}{109}{figure.caption.69}
\contentsline {subsubsection}{Testing the kinematic methodology}{110}{figure.caption.70}
\contentsline {section}{\numberline {4.6}The dynamical masses of NGC\nobreakspace {}147 and NGC\nobreakspace {}185}{112}{section.4.6}
\contentsline {section}{\numberline {4.7}Discussion}{115}{section.4.7}
\contentsline {subsection}{\numberline {4.7.1}Comparison to other dwarf elliptical galaxies}{115}{subsection.4.7.1}
\contentsline {subsection}{\numberline {4.7.2}Comparison to the M31 halo globular clusters}{117}{subsection.4.7.2}
\contentsline {section}{\numberline {4.8}Summary}{118}{section.4.8}
\contentsline {chapter}{\numberline {5}The globular cluster system of NGC\nobreakspace {}6822}{120}{chapter.5}
\contentsline {section}{\numberline {5.1}The data}{123}{section.5.1}
\contentsline {section}{\numberline {5.2}Photometry}{126}{section.5.2}
\contentsline {section}{\numberline {5.3}Ages and metallicities}{130}{section.5.3}
\contentsline {section}{\numberline {5.4}Kinematics}{133}{section.5.4}
\contentsline {section}{\numberline {5.5}The dynamical mass of NGC\nobreakspace {}6822}{134}{section.5.5}
\contentsline {section}{\numberline {5.6}Discussion}{137}{section.5.6}
\contentsline {section}{\numberline {5.7}Summary}{138}{section.5.7}
\contentsline {chapter}{\numberline {6}Summary}{139}{chapter.6}
\contentsline {section}{\numberline {6.1}Summary of science chapters}{139}{section.6.1}
\contentsline {subsubsection}{Kinematics of the M31 outer halo GC system}{139}{section.6.1}
\contentsline {subsubsection}{The GC systems of NGC\nobreakspace {}147 and NGC\nobreakspace {}185}{140}{section.6.1}
\contentsline {subsubsection}{The GC system of NGC\nobreakspace {}6822}{141}{section.6.1}
\contentsline {section}{\numberline {6.2}Future work}{142}{section.6.2}
\contentsline {subsubsection}{The dynamics of the M31 outer halo GC system}{142}{section.6.2}
\contentsline {subsubsection}{The metal content of the M31 halo GCs}{142}{section.6.2}
\contentsline {section}{\numberline {6.3}Final words}{143}{section.6.3}
\contentsline {chapter}{\numberline {A}Radial velocity technique comparison}{144}{appendix.A}
\tocappendix 
\contentsline {chapter}{\numberline {B}Review of the NGC 147 and NGC 185 GC discovery history}{149}{appendix.B}
\contentsline {chapter}{Bibliography}{152}{section*.88}
\contentsfinish 
